/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10filghtsdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Database {

    private Connection dbConn;

    Database() throws SQLException {
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20_filghts?user=root&password=root");

    } // constructor will connect to database, initialize dbConn

    ArrayList<Flight> getAllFlights() throws SQLException {
        try{
            ArrayList<Flight> td = new ArrayList<>();
            PreparedStatement select = dbConn.prepareStatement("select * from flights");
            ResultSet resultSet = select.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                Date onDay = resultSet.getDate("onday");
                String fromcode = resultSet.getString("fromcode");
                String tocode = resultSet.getString("tocode");
                String typeFlight = resultSet.getString("type");
                int passengers = resultSet.getInt("passengers");

                Flight t = new Flight(id,onDay,fromcode,tocode,Flight.typeFlight.valueOf(typeFlight),passengers);
                //int id,String task, int difficulty, Date dueDate, Status status
                td.add(t);
            }
            resultSet.close();
            select.close();
            return td;
        }catch (InvalidValueException|IllegalArgumentException ex) {
            throw new SQLException(ex.getMessage());
        }        
    }
    
    SimpleDateFormat sqldate = new SimpleDateFormat("yyyy-MM-dd");
    void addFlight(Flight flight) throws SQLException {
        try{
            PreparedStatement insert = dbConn.prepareStatement("insert into flights values(NULL,?,?,?,?,?)");
            // `id`,`onday`,`fromcode`,`tocode`,`type`,`passengers`
            insert.setString(1, sqldate.format(flight.getOnday()));
            insert.setString(2, flight.getFromcode());
            insert.setString(3, flight.getTocode());
            insert.setString(4, flight.getType().toString());
            insert.setInt(5, flight.getPassengers());
            insert.execute();
            insert.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    
    void updateFlight(Flight flight) throws SQLException {
        try{
            PreparedStatement update = dbConn.prepareStatement("update flights set onday=?,fromcode=?,tocode=?,type=?,passengers=? where id=?");
            update.setString(1, sqldate.format(flight.getOnday()));
            update.setString(2, flight.getFromcode());
            update.setString(3, flight.getTocode());
            update.setString(4, flight.getType().toString());
            update.setInt(5, flight.getPassengers());
            update.setInt(6, flight.getId());
            update.execute();
            update.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    void deleteFlight(int id) throws SQLException {
        try{
            PreparedStatement delete = dbConn.prepareStatement("delete from flights where id=?");
            delete.setInt(1, id);
            delete.execute();
            delete.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    
}
