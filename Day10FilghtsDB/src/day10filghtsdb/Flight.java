
package day10filghtsdb;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Flight {
    private int id;
    private Date onday;
    private String fromcode;
    private String tocode;
    private typeFlight type;
    private int passengers;
    
    enum typeFlight{Domestic,International,Private};
    
    public Flight(int id, Date onday, String fromcode, String tocode, typeFlight type, int passengers) throws InvalidValueException {
        setId(id);
        setOnday(onday);
        setFromcode(fromcode);
        setTocode(tocode);
        setType(type);
        setPassengers(passengers);
    }

    public int getId() {
        return id;
    }

    public Date getOnday() {
        return onday;
    }

    public String getFromcode() {
        return fromcode;
    }

    public String getTocode() {
        return tocode;
    }

    public typeFlight getType() {
        return type;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOnday(Date onday) {
        this.onday = onday;
    }

    public void setFromcode(String fromcode) throws InvalidValueException {
        if(!fromcode.matches("[A-Z]{3,5}"))
            throw new InvalidValueException("FromCode is not valid");

        this.fromcode = fromcode;
    }

    public void setTocode(String tocode) throws InvalidValueException {
        if(!tocode.matches("[A-Z]{3,5}"))
            throw new InvalidValueException("Tocode is not valid");
        this.tocode = tocode;
    }

    public void setType(typeFlight type) {
        this.type = type;
    }

    public void setPassengers(int passengers) throws InvalidValueException {
        if(200<passengers || passengers<0)
            throw new InvalidValueException("Passengers is not valid");
        this.passengers = passengers;
    }
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    @Override
    public String toString() {
        return String.format("%s %d: from %s to %s on %s has %d passengers.",type, id, fromcode, tocode, dateFormat.format(onday),passengers);
    }  
    
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}