package day06birthdays;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}

public class Birthday {
    private String Name;
    private Date BirthDate;
    
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Birthday(String Name, Date BirthDate) {
        this.Name = Name;
        this.BirthDate = BirthDate;
    }
    
    public Birthday(String dataLine) throws InvalidValueException{
        String[] data = dataLine.split(";");
        if (data.length != 2) {
            throw new InvalidValueException("Invalid number of items in data line"+dataLine);
        }
        try{
            setName(data[0]); // ex NumberFormatException
            setBirthDate(dateFormat.parse(data[1])); // ex ParseException
        }catch(ParseException ex){
            throw new InvalidValueException("Invalid number of items in data line"+dataLine);
        }        
    }
    
    public void setName(String Name) {
        this.Name = Name;
    }

    public void setBirthDate(Date BirthDate) {
        this.BirthDate = BirthDate;
    }

    public String getName() {
        return Name;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    @Override
    public String toString(){
        return String.format("%s bron %s birthday in %d days",Name,dateFormat.format(BirthDate),getBirthdayDays(BirthDate));
    }
    
    String toDataString() {
        return String.format("%s;%s", Name, dateFormat.format(BirthDate));
    }
    
    static public long getBirthdayDays(Date birthday){
        Calendar cToday = Calendar.getInstance(); 
        Calendar cBirth = Calendar.getInstance(); 
        cBirth.setTime(birthday);
        cBirth.set(Calendar.YEAR, cToday.get(Calendar.YEAR)); 
        int days;
        //Period period = Period.between(new now(), LocalDate.MAX);
        
        if (cBirth.get(Calendar.DAY_OF_YEAR) < cToday.get(Calendar.DAY_OF_YEAR)) {
            days = cToday.getActualMaximum(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
            days += cBirth.get(Calendar.DAY_OF_YEAR);
        } else {
            days = cBirth.get(Calendar.DAY_OF_YEAR) - cToday.get(Calendar.DAY_OF_YEAR);
        }
        return days;
    }

    
}
