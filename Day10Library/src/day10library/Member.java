
package day10library;

public class Member {
    private int id;
    private String name;
    private String email;
    private int maxBooksAllowed;
    private byte[] photo;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) throws InvalidValueException {
        if(name.length()<1 || name.length()>100)
            throw new InvalidValueException("Member name must be 1-100 chars");
        this.name = name;
    }

    public void setEmail(String email) throws InvalidValueException {
        if(!email.matches("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$"))
            throw new InvalidValueException("Email must be xxx@xxx.xxx");
        this.email = email;
    }

    public void setMaxBooksAllowed(int maxBooksAllowed) {
        this.maxBooksAllowed = maxBooksAllowed;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Member(int id, String name, String email, int maxBooksAllowed, byte[] photo) throws InvalidValueException {
        setId(id);
        setName(name);
        setEmail(email);
        setMaxBooksAllowed(maxBooksAllowed);
        setPhoto(photo);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getMaxBooksAllowed() {
        return maxBooksAllowed;
    }

    public byte[] getPhoto() {
        return photo;
    }

    @Override
    public String toString() {
        return String.format("%d: %s(%s) max %d books", id, name, email,maxBooksAllowed);
    }   
    
    boolean matches(String search){
        if(this.name.indexOf(search)!=-1)
            return true;

        if(email.indexOf(search)!=-1)
            return true;
        
        String maxBooks = maxBooksAllowed+"";
        if(maxBooks.indexOf(search)!=-1)
            return true;
        
        return false;
    }
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}