/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

/**
 *
 * @author trakadmin
 */
public class Book {
    private int id;
    private String isbn;
    private String authors;
    private String title;
    private int copiesTotal;
    private Genre genre;

    public void setId(int id) {
        this.id = id;
    }

    public void setIsbn(String isbn) throws InvalidValueException {
        if(isbn.length()<1 || isbn.length()>13)
            throw new InvalidValueException("ISBN must be 1-13 chars");
        this.isbn = isbn;
    }

    public void setAuthors(String authors) throws InvalidValueException {
        if(authors.length()<1 || authors.length()>13)
            throw new InvalidValueException("Authors must be 1-200 chars");
        this.authors = authors;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return String.format("%d: %s \"%s\" by %s, %d copies.", id,isbn,title,authors,copiesTotal);
    }

    public void setCopiesTotal(int copiesTotal) {
        this.copiesTotal = copiesTotal;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getAuthors() {
        return authors;
    }

    public String getTitle() {
        return title;
    }

    public int getCopiesTotal() {
        return copiesTotal;
    }

    public Genre getGenre() {
        return genre;
    }
    enum Genre{Fiction, Novel, Mystery, Fantasy, Self_Help};

    public Book(int id, String isbn, String authors, String title, int copiesTotal, Genre genre) throws InvalidValueException {
        setId(id);
        setIsbn(isbn);
        setAuthors(authors);
        setTitle(title);
        setCopiesTotal(copiesTotal);
        setGenre(genre);
    }
    
    boolean matches(String search){
        if(this.isbn.indexOf(search)!=-1)
            return true;

        if(authors.indexOf(search)!=-1)
            return true;
        
        if(title.indexOf(search)!=-1)
            return true;
        
        String maxBooks = copiesTotal+"";
        if(maxBooks.indexOf(search)!=-1)
            return true;
        
        return false;
    }
    
}
