
package day10library;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;


public class Loan {
    private int id;
    private Member member;
    private Book book;
    private Date dateBorrowed;
    private Date dueDate;
    private Date dateReturned;

    public void setId(int id) {
        this.id = id;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setDateBorrowed(Date dateBorrowed) {
        this.dateBorrowed = dateBorrowed;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    
    public void setDateReturned(Date dateReturned) {
        this.dateReturned = dateReturned;
    }

    public Loan(int id, Member member, Book book, Date dateBorrowed, Date dueDate,Date dateReturned) {
        setId(id);
        setMember(member);
        setBook(book);
        setDateBorrowed(dateBorrowed);
        setDueDate(dueDate);
        setDateReturned(dateReturned);
    }

    public int getId() {
        return id;
    }
    
    public String getMemberName() {
        return member.getName();
    }

    public Member getMember() {
        return member;
    }

    public Book getBook() {
        return book;
    }
    
    public String getBookTitle() {
        return book.getTitle();
    }

    public Date getDateBorrowed() {
        return dateBorrowed;
    }

    public Date getDueDate() {
        return dueDate;
    }
    
    public Date getDateReturned() {
        return dateReturned;
    }
    
    public Date getSortReturned() {
        if(dateReturned == null)
            return new Date();
        return dateReturned;
    }

    @Override
    public String toString() {
        String strReturn="";
        //(10 days left)
        
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy");
        if(dateReturned!=null){
            //,returned Dec 10,2020
            strReturn = "returned "+sdf.format(dateReturned);
        }else{
            Date today = new Date();
            if(today.getTime()>dueDate.getTime()){
                strReturn = String.format("due %s, not returned yet(overdue)",sdf.format(dueDate));
            }else{
                strReturn = String.format("due %s(%d days left)",sdf.format(dueDate),(int)((dueDate.getTime() - today.getTime()) / (1000 * 60 * 60 * 24)));
            }
        }
            
        return String.format("%s(#%d) borrowed '%s' by %s on %s, %s", member.getName(),id,book.getTitle(),book.getAuthors(),sdf.format(dateBorrowed),strReturn);
    }

    
    
}
