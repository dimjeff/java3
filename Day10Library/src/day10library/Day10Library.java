/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ipd
 */
public class Day10Library extends javax.swing.JFrame {

    DefaultListModel<Member> modelMemberList = new DefaultListModel<>();
    DefaultListModel<Book> modelBooksList = new DefaultListModel<>();
    DefaultListModel<Loan> modelLoanList = new DefaultListModel<>();
    Database db;

    /**
     * Creates new form Day10Library
     */
    public Day10Library() {
        initComponents();
        dlgBooks_comboGenre.setModel(new DefaultComboBoxModel(Book.Genre.values()));
        try {
            db = new Database();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error connecting to database:\n" + ex.getMessage(),
                    "DB access error",
                    JOptionPane.INFORMATION_MESSAGE);
            System.exit(1);
        }

        //tab head 
        final boolean showTabsHeader = false;
        /*dlgCheckout_tabpane.setUI(new javax.swing.plaf.metal.MetalTabbedPaneUI() {
            @Override
            protected int calculateTabAreaHeight(int tabPlacement, int horizRunCount, int maxTabHeight) {
                if (showTabsHeader) {
                    return super.calculateTabAreaHeight(tabPlacement, horizRunCount, maxTabHeight);
                } else {
                    return 0;
                }
            }

            protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
            }
        });*/
        dlgReturn_tabpane.setUI(new javax.swing.plaf.metal.MetalTabbedPaneUI() {
            @Override
            protected int calculateTabAreaHeight(int tabPlacement, int horizRunCount, int maxTabHeight) {
                if (showTabsHeader) {
                    return super.calculateTabAreaHeight(tabPlacement, horizRunCount, maxTabHeight);
                } else {
                    return 0;
                }
            }

            protected void paintTabArea(Graphics g, int tabPlacement, int selectedIndex) {
            }
        });
    }

    void loadAllMembers(String condition) {
        String filter = digMembers_tfSearch.getText();
        modelMemberList.clear();
        try {
            ArrayList<Member> allMembers = new ArrayList<>(db.getAllMembers(condition));
            if (filter.isEmpty()) {
                modelMemberList.addAll(allMembers);
            } else {
                for (Member p : allMembers) {
                    if (p.matches(filter)) {
                        modelMemberList.addElement(p);
                    }
                }
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error reading from database:\n" + ex.getMessage(),
                    "DB read error",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    void loadAllLoans(String condition, String soryby) {
        modelLoanList.clear();
        try {
            ArrayList<Loan> allLoans = new ArrayList<>(db.getAllLoans(condition));
            Comparator<Loan> comparator = Comparator.comparing(Loan::getId);
            switch (soryby) {
                case "Member's name":
                    comparator = Comparator.comparing(Loan::getMemberName);
                    break;
                case "Book title":
                    comparator = Comparator.comparing(Loan::getBookTitle);
                    break;
                case "Date loaned":
                    comparator = Comparator.comparing(Loan::getDateBorrowed);
                    break;
                case "Date returned":
                    comparator = Comparator.comparing(Loan::getSortReturned);
                    break;
                case "Popularity (total loans)":
                    //????
                    break;
                case "":
                    break;
                default:
                    JOptionPane.showMessageDialog(this,
                            "Error select \n",
                            "select error",
                            JOptionPane.INFORMATION_MESSAGE);
            }
            Collections.sort(allLoans, comparator);
            modelLoanList.addAll(allLoans);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error reading from database:\n" + ex.getMessage(),
                    "DB read error",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    void loadAllBooks(String condition) {
        String filter = dlgBooks_tfFilter.getText();
        modelBooksList.clear();
        try {
            ArrayList<Book> allBooks = new ArrayList<>(db.getAllBooks(condition));
            if (filter.isEmpty()) {
                modelBooksList.addAll(allBooks);
            } else {
                for (Book p : allBooks) {
                    if (p.matches(filter)) {
                        modelBooksList.addElement(p);
                    }
                }
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error reading from database:\n" + ex.getMessage(),
                    "DB read error",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dlgMembers = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        digMembers_tfSearch = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        dlgMembers_lstMemberList = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        dlgMembers_lblId = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        digMembers_lblPhoto = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        digMembers_btAdd = new javax.swing.JButton();
        digMembers_btUpdate = new javax.swing.JButton();
        digMembers_btDelete = new javax.swing.JButton();
        digMembers_tfName = new javax.swing.JTextField();
        digMembers_tfEmail = new javax.swing.JTextField();
        digMembers_comboMaxbooks = new javax.swing.JComboBox<>();
        dlgBooks = new javax.swing.JDialog();
        jLabel9 = new javax.swing.JLabel();
        dlgBooks_tfFilter = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        dlgBooks_lstBooksList = new javax.swing.JList<>();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        dlgBooks_lblId = new javax.swing.JLabel();
        dlgBooks_tfISBN = new javax.swing.JTextField();
        dlgBooks_tfAuthor = new javax.swing.JTextField();
        dlgBooks_tfTitle = new javax.swing.JTextField();
        dlgBooks_spTotalCopies = new javax.swing.JSpinner();
        dlgBooks_comboGenre = new javax.swing.JComboBox<>();
        dlgBooks_btAdd = new javax.swing.JButton();
        dlgBooks_btUpdate = new javax.swing.JButton();
        dlgBooks_btDelete = new javax.swing.JButton();
        dlgCheckout = new javax.swing.JDialog();
        dlgCheckout_tabpane = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        dlgCheckout_lstMemberList = new javax.swing.JList<>();
        dlgCheckout_btBack = new javax.swing.JButton();
        dlgCheckout_btNext1 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        dlgCheckout_lstBookList = new javax.swing.JList<>();
        dlgCheckout_btBack2 = new javax.swing.JButton();
        dlgCheckout_btNext2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        dlgCheckout_lblMemberDesc = new javax.swing.JLabel();
        dlgCheckout_lblBookDesc = new javax.swing.JLabel();
        dlgCheckout_btBack3 = new javax.swing.JButton();
        dlgCheckout_btFinish = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jRadioButton5 = new javax.swing.JRadioButton();
        dlgReturn = new javax.swing.JDialog();
        dlgReturn_tabpane = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        dlgReturn_lstMemberList = new javax.swing.JList<>();
        dlgReturn_btBack1 = new javax.swing.JButton();
        dlgReturn_btNext1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        dlgReturn_lstLoanList = new javax.swing.JList<>();
        dlgReturn_btBack2 = new javax.swing.JButton();
        dlgReturn_btNext2 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        dlgReturn_lblMemberDesc = new javax.swing.JLabel();
        dlgReturn_lblBookDesc = new javax.swing.JLabel();
        dlgReturn_btBack3 = new javax.swing.JButton();
        dlgReturn_btFinish = new javax.swing.JButton();
        jLabel25 = new javax.swing.JLabel();
        dlgLoaned = new javax.swing.JDialog();
        jLabel23 = new javax.swing.JLabel();
        dlgLoaned_comboSortby = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        dlgLoaned_comboShow = new javax.swing.JComboBox<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        dlgLoaned_lstLoanList = new javax.swing.JList<>();
        jButton15 = new javax.swing.JButton();
        filechooser = new javax.swing.JFileChooser();
        buttongroup = new javax.swing.ButtonGroup();
        btManageMembers = new javax.swing.JButton();
        btManageBooks = new javax.swing.JButton();
        btCheckoutBook = new javax.swing.JButton();
        btReturnBook = new javax.swing.JButton();
        btViewBooksLoaned = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        dlgMembers.setModal(true);

        jLabel1.setText("Search:");

        digMembers_tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                digMembers_tfSearchKeyReleased(evt);
            }
        });

        jButton6.setText("Clear");

        dlgMembers_lstMemberList.setModel(modelMemberList);
        dlgMembers_lstMemberList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgMembers_lstMemberListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(dlgMembers_lstMemberList);

        jLabel2.setText("Id: ");

        dlgMembers_lblId.setText("-");

        jLabel4.setText("Name:");

        jLabel5.setText("Email:");

        jLabel6.setText("Max books:");

        digMembers_lblPhoto.setText("150x150 pixels, clickable  ");
        digMembers_lblPhoto.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        digMembers_lblPhoto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                digMembers_lblPhotoMouseClicked(evt);
            }
        });

        jLabel8.setText("Photo:");

        digMembers_btAdd.setText("Add");
        digMembers_btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                digMembers_btAddActionPerformed(evt);
            }
        });

        digMembers_btUpdate.setText("Update");
        digMembers_btUpdate.setEnabled(false);
        digMembers_btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                digMembers_btUpdateActionPerformed(evt);
            }
        });

        digMembers_btDelete.setText("Delete");
        digMembers_btDelete.setEnabled(false);
        digMembers_btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                digMembers_btDeleteActionPerformed(evt);
            }
        });

        digMembers_tfName.setText("Jerry");

        digMembers_tfEmail.setText("jerry@xyz.com");

        digMembers_comboMaxbooks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- please select --", "3", "5", "10", "15", "20" }));

        javax.swing.GroupLayout dlgMembersLayout = new javax.swing.GroupLayout(dlgMembers.getContentPane());
        dlgMembers.getContentPane().setLayout(dlgMembersLayout);
        dlgMembersLayout.setHorizontalGroup(
            dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgMembersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgMembersLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(dlgMembersLayout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(digMembers_tfName))
                            .addGroup(dlgMembersLayout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(digMembers_tfEmail))
                            .addGroup(dlgMembersLayout.createSequentialGroup()
                                .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(dlgMembersLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(digMembers_lblPhoto)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(dlgMembersLayout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(digMembers_comboMaxbooks, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(dlgMembersLayout.createSequentialGroup()
                                .addComponent(digMembers_btAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(digMembers_btUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(digMembers_btDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(dlgMembersLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(digMembers_tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton6)
                        .addGap(85, 85, 85)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dlgMembers_lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgMembersLayout.setVerticalGroup(
            dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgMembersLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(digMembers_tfSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6)
                    .addComponent(jLabel2)
                    .addComponent(dlgMembers_lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(dlgMembersLayout.createSequentialGroup()
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(digMembers_tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(digMembers_tfEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(digMembers_comboMaxbooks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(digMembers_lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgMembersLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(digMembers_btAdd)
                            .addComponent(digMembers_btUpdate)
                            .addComponent(digMembers_btDelete))
                        .addGap(0, 5, Short.MAX_VALUE)))
                .addContainerGap())
        );

        dlgBooks.setModal(true);

        jLabel9.setText("Search:");

        dlgBooks_tfFilter.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dlgBooks_tfFilterKeyReleased(evt);
            }
        });

        jButton1.setText("Clear");

        dlgBooks_lstBooksList.setModel(modelBooksList);
        dlgBooks_lstBooksList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgBooks_lstBooksList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgBooks_lstBooksListValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(dlgBooks_lstBooksList);

        jLabel10.setText("Id: ");

        jLabel11.setText("Authors:");

        jLabel12.setText("ISBN:");

        jLabel13.setText("Title:");

        jLabel14.setText("Genre:");

        jLabel15.setText("Copies total:");

        dlgBooks_lblId.setText("-");

        dlgBooks_spTotalCopies.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                dlgBooks_spTotalCopiesStateChanged(evt);
            }
        });

        dlgBooks_comboGenre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fiction", "Novel", "Mystery", "Fantasy", "Self-Help" }));

        dlgBooks_btAdd.setText("Add");
        dlgBooks_btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgBooks_btAddActionPerformed(evt);
            }
        });

        dlgBooks_btUpdate.setText("Update");
        dlgBooks_btUpdate.setEnabled(false);
        dlgBooks_btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgBooks_btUpdateActionPerformed(evt);
            }
        });

        dlgBooks_btDelete.setText("Delete");
        dlgBooks_btDelete.setEnabled(false);
        dlgBooks_btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgBooks_btDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout dlgBooksLayout = new javax.swing.GroupLayout(dlgBooks.getContentPane());
        dlgBooks.getContentPane().setLayout(dlgBooksLayout);
        dlgBooksLayout.setHorizontalGroup(
            dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBooksLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgBooks_tfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgBooks_tfISBN))
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgBooks_tfAuthor))
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgBooks_tfTitle))
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dlgBooks_comboGenre, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(dlgBooksLayout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dlgBooks_lblId, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(dlgBooksLayout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dlgBooks_spTotalCopies, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(dlgBooksLayout.createSequentialGroup()
                                .addComponent(dlgBooks_btAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(dlgBooks_btUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(dlgBooks_btDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        dlgBooksLayout.setVerticalGroup(
            dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgBooksLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(dlgBooks_tfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel10)
                    .addComponent(dlgBooks_lblId))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(dlgBooksLayout.createSequentialGroup()
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(dlgBooks_tfISBN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(dlgBooks_tfAuthor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(dlgBooks_tfTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(dlgBooks_spTotalCopies, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(dlgBooks_comboGenre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30)
                        .addGroup(dlgBooksLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dlgBooks_btAdd)
                            .addComponent(dlgBooks_btUpdate)
                            .addComponent(dlgBooks_btDelete))
                        .addGap(0, 52, Short.MAX_VALUE)))
                .addContainerGap())
        );

        dlgCheckout.setModal(true);

        dlgCheckout_lstMemberList.setModel(modelMemberList);
        dlgCheckout_lstMemberList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgCheckout_lstMemberList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgCheckout_lstMemberListValueChanged(evt);
            }
        });
        jScrollPane3.setViewportView(dlgCheckout_lstMemberList);

        dlgCheckout_btBack.setText("Back");
        dlgCheckout_btBack.setEnabled(false);
        dlgCheckout_btBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btBackActionPerformed(evt);
            }
        });

        dlgCheckout_btNext1.setText("Next");
        dlgCheckout_btNext1.setEnabled(false);
        dlgCheckout_btNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(dlgCheckout_btBack, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(dlgCheckout_btNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgCheckout_btBack)
                    .addComponent(dlgCheckout_btNext1))
                .addContainerGap())
        );

        dlgCheckout_tabpane.addTab("Choose member", jPanel1);

        dlgCheckout_lstBookList.setModel(modelBooksList);
        dlgCheckout_lstBookList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgCheckout_lstBookListValueChanged(evt);
            }
        });
        jScrollPane4.setViewportView(dlgCheckout_lstBookList);

        dlgCheckout_btBack2.setText("Back");
        dlgCheckout_btBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btBack2ActionPerformed(evt);
            }
        });

        dlgCheckout_btNext2.setText("Next");
        dlgCheckout_btNext2.setEnabled(false);
        dlgCheckout_btNext2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btNext2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dlgCheckout_btBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(dlgCheckout_btNext2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgCheckout_btBack2)
                    .addComponent(dlgCheckout_btNext2))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        dlgCheckout_tabpane.addTab("Choose book", jPanel2);

        jLabel17.setText("Book chosen:");

        jLabel18.setText("Member chosen:");

        dlgCheckout_lblMemberDesc.setText("3: Theresa (trm@gmail.com) max 10 books");

        dlgCheckout_lblBookDesc.setText("3: 2873648723X \"Corn at sunrise\" by Jerry Boe, 5 copies");

        dlgCheckout_btBack3.setText("Back");
        dlgCheckout_btBack3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btBack3ActionPerformed(evt);
            }
        });

        dlgCheckout_btFinish.setText("Finish");
        dlgCheckout_btFinish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgCheckout_btFinishActionPerformed(evt);
            }
        });

        jLabel21.setText("Pressing finish will loan the book.");

        jLabel22.setText("To be returned in this many days:");

        buttongroup.add(jRadioButton1);
        jRadioButton1.setText("10");

        buttongroup.add(jRadioButton2);
        jRadioButton2.setText("20");

        buttongroup.add(jRadioButton3);
        jRadioButton3.setSelected(true);
        jRadioButton3.setText("30");

        buttongroup.add(jRadioButton4);
        jRadioButton4.setText("60");

        buttongroup.add(jRadioButton5);
        jRadioButton5.setText("90");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(dlgCheckout_btBack3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dlgCheckout_btFinish, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgCheckout_lblMemberDesc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dlgCheckout_lblBookDesc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18)
                            .addComponent(jLabel17)
                            .addComponent(jLabel22)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jRadioButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton2)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton3)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton4)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton5)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dlgCheckout_lblMemberDesc)
                .addGap(18, 18, 18)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dlgCheckout_lblBookDesc)
                .addGap(26, 26, 26)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton2)
                    .addComponent(jRadioButton3)
                    .addComponent(jRadioButton4)
                    .addComponent(jRadioButton5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jLabel21)
                .addGap(27, 27, 27)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgCheckout_btBack3)
                    .addComponent(dlgCheckout_btFinish))
                .addContainerGap())
        );

        dlgCheckout_tabpane.addTab("Confirm", jPanel3);

        javax.swing.GroupLayout dlgCheckoutLayout = new javax.swing.GroupLayout(dlgCheckout.getContentPane());
        dlgCheckout.getContentPane().setLayout(dlgCheckoutLayout);
        dlgCheckoutLayout.setHorizontalGroup(
            dlgCheckoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dlgCheckout_tabpane)
        );
        dlgCheckoutLayout.setVerticalGroup(
            dlgCheckoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dlgCheckout_tabpane)
        );

        dlgReturn.setModal(true);

        dlgReturn_lstMemberList.setModel(modelMemberList);
        dlgReturn_lstMemberList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dlgReturn_lstMemberList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgReturn_lstMemberListValueChanged(evt);
            }
        });
        jScrollPane6.setViewportView(dlgReturn_lstMemberList);

        dlgReturn_btBack1.setText("Back");
        dlgReturn_btBack1.setEnabled(false);
        dlgReturn_btBack1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btBack1ActionPerformed(evt);
            }
        });

        dlgReturn_btNext1.setText("Next");
        dlgReturn_btNext1.setEnabled(false);
        dlgReturn_btNext1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btNext1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(dlgReturn_btBack1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(dlgReturn_btNext1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgReturn_btBack1)
                    .addComponent(dlgReturn_btNext1))
                .addContainerGap())
        );

        dlgReturn_tabpane.addTab("Choose member", jPanel4);

        dlgReturn_lstLoanList.setModel(modelLoanList);
        dlgReturn_lstLoanList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                dlgReturn_lstLoanListValueChanged(evt);
            }
        });
        jScrollPane7.setViewportView(dlgReturn_lstLoanList);

        dlgReturn_btBack2.setText("Back");
        dlgReturn_btBack2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btBack2ActionPerformed(evt);
            }
        });

        dlgReturn_btNext2.setText("Next");
        dlgReturn_btNext2.setEnabled(false);
        dlgReturn_btNext2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btNext2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dlgReturn_btBack2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(dlgReturn_btNext2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgReturn_btBack2)
                    .addComponent(dlgReturn_btNext2))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        dlgReturn_tabpane.addTab("Choose book", jPanel5);

        jLabel19.setText("Book chosen:");

        jLabel20.setText("Member chosen:");

        dlgReturn_lblMemberDesc.setText("3: Theresa (trm@gmail.com) max 10 books");

        dlgReturn_lblBookDesc.setText("3: 2873648723X \"Corn at sunrise\" by Jerry Boe, 5 copies");

        dlgReturn_btBack3.setText("Back");
        dlgReturn_btBack3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btBack3ActionPerformed(evt);
            }
        });

        dlgReturn_btFinish.setText("Finish");
        dlgReturn_btFinish.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgReturn_btFinishActionPerformed(evt);
            }
        });

        jLabel25.setText("Pressing finish will loan the book.");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(dlgReturn_btBack3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dlgReturn_btFinish, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dlgReturn_lblMemberDesc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dlgReturn_lblBookDesc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addComponent(jLabel19))
                        .addGap(0, 296, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dlgReturn_lblMemberDesc)
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(dlgReturn_lblBookDesc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                .addComponent(jLabel25)
                .addGap(27, 27, 27)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgReturn_btBack3)
                    .addComponent(dlgReturn_btFinish))
                .addContainerGap())
        );

        dlgReturn_tabpane.addTab("Confirm", jPanel6);

        javax.swing.GroupLayout dlgReturnLayout = new javax.swing.GroupLayout(dlgReturn.getContentPane());
        dlgReturn.getContentPane().setLayout(dlgReturnLayout);
        dlgReturnLayout.setHorizontalGroup(
            dlgReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dlgReturn_tabpane)
        );
        dlgReturnLayout.setVerticalGroup(
            dlgReturnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dlgReturn_tabpane)
        );

        dlgLoaned.setModal(true);

        jLabel23.setText("Sort by:");

        dlgLoaned_comboSortby.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Member's name", "Book title", "Date loaned", "Date returned", "Popularity (total loans)" }));
        dlgLoaned_comboSortby.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                dlgLoaned_comboSortbyItemStateChanged(evt);
            }
        });

        jLabel24.setText("Show:");

        dlgLoaned_comboShow.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Currently loaned", "Overdue only", "All past and present loans" }));
        dlgLoaned_comboShow.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                dlgLoaned_comboShowItemStateChanged(evt);
            }
        });

        dlgLoaned_lstLoanList.setModel(modelLoanList);
        jScrollPane5.setViewportView(dlgLoaned_lstLoanList);

        jButton15.setText("Dismiss");

        javax.swing.GroupLayout dlgLoanedLayout = new javax.swing.GroupLayout(dlgLoaned.getContentPane());
        dlgLoaned.getContentPane().setLayout(dlgLoanedLayout);
        dlgLoanedLayout.setHorizontalGroup(
            dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgLoanedLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 665, Short.MAX_VALUE)
                    .addGroup(dlgLoanedLayout.createSequentialGroup()
                        .addGroup(dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dlgLoaned_comboSortby, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dlgLoaned_comboShow, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        dlgLoanedLayout.setVerticalGroup(
            dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgLoanedLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(dlgLoaned_comboShow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton15))
                .addGap(18, 18, 18)
                .addGroup(dlgLoanedLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(dlgLoaned_comboSortby, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btManageMembers.setText("Manage members");
        btManageMembers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btManageMembersActionPerformed(evt);
            }
        });

        btManageBooks.setText("Manage books");
        btManageBooks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btManageBooksActionPerformed(evt);
            }
        });

        btCheckoutBook.setText("Checkout book");
        btCheckoutBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btCheckoutBookActionPerformed(evt);
            }
        });

        btReturnBook.setText("Return book");
        btReturnBook.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btReturnBookActionPerformed(evt);
            }
        });

        btViewBooksLoaned.setText("View books loaned");
        btViewBooksLoaned.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btViewBooksLoanedActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        jMenuItem1.setText("Exit");
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btManageMembers, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btManageBooks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btCheckoutBook, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                    .addComponent(btReturnBook, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btViewBooksLoaned, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btManageMembers)
                    .addComponent(btCheckoutBook)
                    .addComponent(btViewBooksLoaned))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btManageBooks)
                    .addComponent(btReturnBook))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btManageMembersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btManageMembersActionPerformed
        loadAllMembers("");
        dlgMembers.pack();
        dlgMembers.setVisible(true);
    }//GEN-LAST:event_btManageMembersActionPerformed

    private void btManageBooksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btManageBooksActionPerformed
        loadAllBooks("");
        dlgBooks.pack();
        dlgBooks.setVisible(true);
    }//GEN-LAST:event_btManageBooksActionPerformed

    private void btCheckoutBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btCheckoutBookActionPerformed
        digMembers_tfSearch.setText("");
        dlgCheckout_tabpane.setSelectedIndex(0);
        dlgCheckout_tabpane.setEnabledAt(0, false);
        dlgCheckout_tabpane.setEnabledAt(1, false);
        dlgCheckout_tabpane.setEnabledAt(2, false);
        loadAllMembers("");
        dlgCheckout.pack();
        dlgCheckout.setVisible(true);
    }//GEN-LAST:event_btCheckoutBookActionPerformed

    private void btReturnBookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btReturnBookActionPerformed
        digMembers_tfSearch.setText("");
        dlgReturn_tabpane.setSelectedIndex(0);
        loadAllMembers(" where id in(select memberId from loans where dateReturned is null)");
        dlgReturn.pack();
        dlgReturn.setVisible(true);
    }//GEN-LAST:event_btReturnBookActionPerformed

    private void btViewBooksLoanedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btViewBooksLoanedActionPerformed
        loadAllLoans("", "Member's name");
        dlgLoaned_comboShow.setSelectedIndex(2);
        dlgLoaned.pack();
        dlgLoaned.setVisible(true);
    }//GEN-LAST:event_btViewBooksLoanedActionPerformed

    private void digMembers_lblPhotoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_digMembers_lblPhotoMouseClicked
        filechooser.setFileFilter(new FileNameExtensionFilter("Image files (*.jpg,*.png,*.gif,*.bmp)", "jpg", "png", "gif", "bmp"));
        if (filechooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {

            try {
                BufferedImage img = ImageIO.read(filechooser.getSelectedFile());
                int[] imgwh = getWidthHeight(img.getWidth(), img.getHeight());
                Image dimg = img.getScaledInstance(imgwh[0], imgwh[1], Image.SCALE_SMOOTH);
                ImageIcon imageIcon = new ImageIcon(dimg);
                digMembers_lblPhoto.setText("");
                digMembers_lblPhoto.setIcon(imageIcon);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(this,
                        "Error display image file:\n" + e.getMessage(),
                        "File read error",
                        JOptionPane.INFORMATION_MESSAGE);
            }

        }
    }//GEN-LAST:event_digMembers_lblPhotoMouseClicked

    int[] getWidthHeight(int w, int h) {
        int[] imgWH = new int[2];
        int width = 0;
        int height = 0;
        if (w <= 150 && h <= 150) {
            imgWH[0] = w;
            imgWH[1] = h;

        } else {
            int max = w > h ? w : h;
            imgWH[0] = (int) ((double) w / ((double) max / 150));
            imgWH[1] = (int) ((double) h / ((double) max / 150));
        }
        return imgWH;
    }

    private void dlgMembers_lstMemberListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgMembers_lstMemberListValueChanged
        if (dlgMembers_lstMemberList.getSelectedIndex() != -1) {
            Member member = dlgMembers_lstMemberList.getSelectedValue();
            digMembers_lblPhoto.setText("150x150 pixels, clickable  ");
            digMembers_lblPhoto.setIcon(null);
            byte[] byteArray = member.getPhoto();
            if (byteArray != null) {
                ImageIcon imageIcon = new ImageIcon(byteArray);
                imageIcon.getImage();
                digMembers_lblPhoto.setText("");
                digMembers_lblPhoto.setIcon(imageIcon);
            }
            digMembers_tfName.setText(member.getName());
            digMembers_tfEmail.setText(member.getEmail());
            dlgMembers_lblId.setText(member.getId() + "");
            digMembers_comboMaxbooks.setSelectedItem(member.getMaxBooksAllowed() + "");
            digMembers_btDelete.setEnabled(true);
            digMembers_btUpdate.setEnabled(true);
        }
    }//GEN-LAST:event_dlgMembers_lstMemberListValueChanged

    private void digMembers_btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_digMembers_btAddActionPerformed
        try {
            // id,name,email,maxBooksAllowed,currentBooksRented,photo
            String name = digMembers_tfName.getText();
            String email = digMembers_tfEmail.getText();
            int maxBooksAllowed = Integer.valueOf(digMembers_comboMaxbooks.getSelectedItem().toString());

            Icon icon = digMembers_lblPhoto.getIcon();
            Member member;
            byte[] buf = null;
            if (icon != null) {
                Image img = ((ImageIcon) icon).getImage();
                BufferedImage bimg = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TRANSLUCENT);
                Graphics g = bimg.getGraphics();
                g.drawImage(img, 0, 0, null);
                g.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bimg, "png", baos);
                buf = baos.toByteArray();                
            }
            member = new Member(0, name, email, maxBooksAllowed, buf);

            db.addMember(member);
            loadAllMembers("");
        } catch (InvalidValueException | SQLException | IOException | NumberFormatException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_digMembers_btAddActionPerformed

    private void digMembers_btUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_digMembers_btUpdateActionPerformed
        try {
            // id,name,email,maxBooksAllowed,currentBooksRented,photo
            int id = Integer.valueOf(dlgMembers_lblId.getText());
            String name = digMembers_tfName.getText();
            String email = digMembers_tfEmail.getText();
            int maxBooksAllowed = Integer.valueOf(digMembers_comboMaxbooks.getSelectedItem().toString());

            Icon icon = digMembers_lblPhoto.getIcon();
            Image img = ((ImageIcon) icon).getImage();
            //File file = new File("test.png");
            BufferedImage bimg = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TRANSLUCENT);
            Graphics g = bimg.getGraphics();
            g.drawImage(img, 0, 0, null);
            g.dispose();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            //ImageIO.write(bimg, "png", file);
            ImageIO.write(bimg, "png", baos);
            byte[] buf = baos.toByteArray();

            Member member = new Member(id, name, email, maxBooksAllowed, buf);

            db.updateMember(member);
            loadAllMembers("");
        } catch (InvalidValueException | SQLException | IOException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_digMembers_btUpdateActionPerformed

    private void digMembers_btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_digMembers_btDeleteActionPerformed
        if (dlgMembers_lstMemberList.getSelectedIndex() != -1) {
            int input = JOptionPane.showConfirmDialog(null, "Are you sure to delete?");
            // 0=yes, 1=no, 2=cancel
            if (input == 0)
                try {
                db.deleteMember(dlgMembers_lstMemberList.getSelectedValue().getId());
                loadAllMembers("");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error to delete record:\n" + ex.getMessage(),
                        "DB error",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_digMembers_btDeleteActionPerformed

    private void digMembers_tfSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_digMembers_tfSearchKeyReleased
        loadAllMembers("");
    }//GEN-LAST:event_digMembers_tfSearchKeyReleased

    private void dlgBooks_spTotalCopiesStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_dlgBooks_spTotalCopiesStateChanged
        if ((int) dlgBooks_spTotalCopies.getValue() < 0) {
            JOptionPane.showMessageDialog(this,
                    "The number can not be less then Zero.\n",
                    "DB error",
                    JOptionPane.INFORMATION_MESSAGE);
            dlgBooks_spTotalCopies.setValue(0);
        }
    }//GEN-LAST:event_dlgBooks_spTotalCopiesStateChanged

    private void dlgBooks_btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgBooks_btAddActionPerformed
        try {
            //id,isbn,authors,title,copiesTotal,currentCopiesRented,genre
            String ISBN = dlgBooks_tfISBN.getText();
            String Author = dlgBooks_tfAuthor.getText();
            String Title = dlgBooks_tfTitle.getText();
            int TotalCopies = Integer.valueOf(dlgBooks_spTotalCopies.getValue().toString());
            String Genre = dlgBooks_comboGenre.getSelectedItem().toString();

            Book book = new Book(0, ISBN, Author, Title, TotalCopies, Book.Genre.valueOf(Genre));

            db.addBook(book);
            loadAllBooks("");
        } catch (InvalidValueException | SQLException | IOException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgBooks_btAddActionPerformed

    private void dlgBooks_btUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgBooks_btUpdateActionPerformed
        try {
            //id,isbn,authors,title,copiesTotal,currentCopiesRented,genre
            int id = Integer.valueOf(dlgBooks_lblId.getText());
            String ISBN = dlgBooks_tfISBN.getText();
            String Author = dlgBooks_tfAuthor.getText();
            String Title = dlgBooks_tfTitle.getText();
            int TotalCopies = Integer.valueOf(dlgBooks_spTotalCopies.getValue().toString());
            String Genre = dlgBooks_comboGenre.getSelectedItem().toString();

            Book book = new Book(id, ISBN, Author, Title, TotalCopies, Book.Genre.valueOf(Genre));

            db.updateBook(book);
            loadAllBooks("");
        } catch (InvalidValueException | SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_dlgBooks_btUpdateActionPerformed

    private void dlgBooks_btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgBooks_btDeleteActionPerformed
        if (dlgBooks_lstBooksList.getSelectedIndex() != -1) {
            int input = JOptionPane.showConfirmDialog(null, "Are you sure to delete?");
            // 0=yes, 1=no, 2=cancel
            if (input == 0)
                try {
                db.deleteBook(dlgBooks_lstBooksList.getSelectedValue().getId());
                loadAllBooks("");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this,
                        "Error to delete record:\n" + ex.getMessage(),
                        "DB error",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }//GEN-LAST:event_dlgBooks_btDeleteActionPerformed

    private void dlgBooks_lstBooksListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgBooks_lstBooksListValueChanged
        if (dlgBooks_lstBooksList.getSelectedIndex() != -1) {
            Book book = dlgBooks_lstBooksList.getSelectedValue();

            dlgBooks_lblId.setText(book.getId() + "");
            dlgBooks_tfISBN.setText(book.getIsbn());
            dlgBooks_tfAuthor.setText(book.getAuthors());
            dlgBooks_tfTitle.setText(book.getTitle());
            dlgBooks_spTotalCopies.setValue(book.getCopiesTotal());
            dlgBooks_comboGenre.setSelectedItem(book.getGenre());
            dlgBooks_btDelete.setEnabled(true);
            dlgBooks_btUpdate.setEnabled(true);
        }
    }//GEN-LAST:event_dlgBooks_lstBooksListValueChanged

    private void dlgBooks_tfFilterKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dlgBooks_tfFilterKeyReleased
        loadAllBooks("");
    }//GEN-LAST:event_dlgBooks_tfFilterKeyReleased

    private void dlgCheckout_lstMemberListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgCheckout_lstMemberListValueChanged
        if (dlgCheckout_lstMemberList.getSelectedIndex() != -1)
            dlgCheckout_btNext1.setEnabled(true);
        else
            dlgCheckout_btNext1.setEnabled(false);
    }//GEN-LAST:event_dlgCheckout_lstMemberListValueChanged

    private void dlgCheckout_btNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btNext1ActionPerformed
        Member member = dlgCheckout_lstMemberList.getSelectedValue();
        int id = member.getId();
        try {
            if (!db.memberEligibleCheckout(id)) {
                JOptionPane.showMessageDialog(this,
                        "The member is not eligible to checkout\n",
                        "Member error",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            dlgCheckout_lblMemberDesc.setText(member.toString());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error to access database\n" + ex.getMessage(),
                    "DB error",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        dlgBooks_tfFilter.setText("");
        loadAllBooks("");
        dlgCheckout_tabpane.setSelectedIndex(dlgCheckout_tabpane.getSelectedIndex() + 1);
    }//GEN-LAST:event_dlgCheckout_btNext1ActionPerformed

    private void dlgCheckout_btBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btBackActionPerformed
        dlgCheckout_tabpane.setSelectedIndex(dlgCheckout_tabpane.getSelectedIndex() - 1);
    }//GEN-LAST:event_dlgCheckout_btBackActionPerformed

    private void dlgCheckout_lstBookListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgCheckout_lstBookListValueChanged
        if (dlgCheckout_lstBookList.getSelectedIndex() != -1) {
            dlgCheckout_btNext2.setEnabled(true);
        } else
            dlgCheckout_btNext2.setEnabled(false);
    }//GEN-LAST:event_dlgCheckout_lstBookListValueChanged

    private void dlgCheckout_btBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btBack2ActionPerformed
        dlgCheckout_tabpane.setSelectedIndex(dlgCheckout_tabpane.getSelectedIndex() - 1);
    }//GEN-LAST:event_dlgCheckout_btBack2ActionPerformed

    private void dlgCheckout_btNext2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btNext2ActionPerformed
        Book book = dlgCheckout_lstBookList.getSelectedValue();
        int id = book.getId();
        try {
            if (!db.bookEligibleCheckout(id)) {
                JOptionPane.showMessageDialog(this,
                        "The book is not loanable to checkout\n",
                        "Book error",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            dlgCheckout_lblBookDesc.setText(book.toString());
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error to access database\n" + ex.getMessage(),
                    "DB error",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        dlgCheckout_tabpane.setSelectedIndex(dlgCheckout_tabpane.getSelectedIndex() + 1);
    }//GEN-LAST:event_dlgCheckout_btNext2ActionPerformed

    private void dlgCheckout_btBack3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btBack3ActionPerformed
        dlgCheckout_tabpane.setSelectedIndex(dlgCheckout_tabpane.getSelectedIndex() - 1);
    }//GEN-LAST:event_dlgCheckout_btBack3ActionPerformed

    private void dlgCheckout_btFinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgCheckout_btFinishActionPerformed
        Member member = dlgCheckout_lstMemberList.getSelectedValue();
        Book book = dlgCheckout_lstBookList.getSelectedValue();
        int dueDate = 0;
        for (Enumeration<AbstractButton> buttons = buttongroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                dueDate = Integer.valueOf(button.getText());
                break;
            }
        }
        try {
            db.bookCheckout(member.getId(), book.getId(), dueDate);
            dlgCheckout.setVisible(false);
            dlgCheckout.dispose();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error to access database\n" + ex.getMessage(),
                    "DB error",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_dlgCheckout_btFinishActionPerformed

    private void dlgLoaned_comboShowItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dlgLoaned_comboShowItemStateChanged
        String condition = dlgLoaned_comboShow.getSelectedItem().toString();
        condition = getCondition(condition);
        if (condition == null) {
            JOptionPane.showMessageDialog(this,
                    "Error to select\n",
                    "Select error",
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        String sortby = dlgLoaned_comboSortby.getSelectedItem().toString();
        loadAllLoans(condition, sortby);
    }//GEN-LAST:event_dlgLoaned_comboShowItemStateChanged

    private void dlgLoaned_comboSortbyItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dlgLoaned_comboSortbyItemStateChanged
        String sortby = dlgLoaned_comboSortby.getSelectedItem().toString();
        String condition = dlgLoaned_comboShow.getSelectedItem().toString();
        condition = getCondition(condition);
        loadAllLoans(condition, sortby);
    }//GEN-LAST:event_dlgLoaned_comboSortbyItemStateChanged

    private void dlgReturn_lstMemberListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgReturn_lstMemberListValueChanged
        if (dlgReturn_lstMemberList.getSelectedIndex() != -1)
            dlgReturn_btNext1.setEnabled(true);
        else
            dlgReturn_btNext1.setEnabled(false);
    }//GEN-LAST:event_dlgReturn_lstMemberListValueChanged

    private void dlgReturn_btBack1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btBack1ActionPerformed

    }//GEN-LAST:event_dlgReturn_btBack1ActionPerformed

    private void dlgReturn_btNext1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btNext1ActionPerformed
        Member member = dlgReturn_lstMemberList.getSelectedValue();
        dlgReturn_lblMemberDesc.setText(member.toString());
        loadAllLoans(String.format("l.memberId=%d and l.dateReturned is null", member.getId()), "");
        //loadAllBooks(String.format(" where id in(select bookid from loans where memberId=%d and dateReturned is null)", dlgReturn_lstMemberList.getSelectedValue().getId()));
        dlgReturn_tabpane.setSelectedIndex(dlgReturn_tabpane.getSelectedIndex() + 1);
    }//GEN-LAST:event_dlgReturn_btNext1ActionPerformed

    private void dlgReturn_lstLoanListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_dlgReturn_lstLoanListValueChanged
        if (dlgReturn_lstLoanList.getSelectedIndex() != -1) {
            dlgReturn_btNext2.setEnabled(true);
        } else
            dlgReturn_btNext2.setEnabled(false);
    }//GEN-LAST:event_dlgReturn_lstLoanListValueChanged

    private void dlgReturn_btBack2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btBack2ActionPerformed
        dlgReturn_tabpane.setSelectedIndex(dlgReturn_tabpane.getSelectedIndex() - 1);
    }//GEN-LAST:event_dlgReturn_btBack2ActionPerformed

    private void dlgReturn_btNext2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btNext2ActionPerformed
        Loan loan = dlgReturn_lstLoanList.getSelectedValue();
        dlgReturn_lblBookDesc.setText("'" + loan.getBook().getTitle() + "' on " + loan.getDateBorrowed());
        dlgReturn_tabpane.setSelectedIndex(dlgReturn_tabpane.getSelectedIndex() + 1);
    }//GEN-LAST:event_dlgReturn_btNext2ActionPerformed

    private void dlgReturn_btBack3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btBack3ActionPerformed
        dlgReturn_tabpane.setSelectedIndex(dlgReturn_tabpane.getSelectedIndex() - 1);
    }//GEN-LAST:event_dlgReturn_btBack3ActionPerformed

    private void dlgReturn_btFinishActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgReturn_btFinishActionPerformed
        Loan loan = dlgReturn_lstLoanList.getSelectedValue();
        try {
            db.bookReturn(loan);
            dlgReturn.setVisible(false);
            dlgReturn.dispose();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Error to access database\n" + ex.getMessage(),
                    "DB error",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_dlgReturn_btFinishActionPerformed

    String getCondition(String condition) {
        switch (condition) {
            case "Currently loaned":
                return "l.dueDate>now()";
            case "Overdue only":
                return "l.dateReturned is null and l.dueDate<now()";
            case "All past and present loans":
                return "";
            default:
                return null;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day10Library.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day10Library().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btCheckoutBook;
    private javax.swing.JButton btManageBooks;
    private javax.swing.JButton btManageMembers;
    private javax.swing.JButton btReturnBook;
    private javax.swing.JButton btViewBooksLoaned;
    private javax.swing.ButtonGroup buttongroup;
    private javax.swing.JButton digMembers_btAdd;
    private javax.swing.JButton digMembers_btDelete;
    private javax.swing.JButton digMembers_btUpdate;
    private javax.swing.JComboBox<String> digMembers_comboMaxbooks;
    private javax.swing.JLabel digMembers_lblPhoto;
    private javax.swing.JTextField digMembers_tfEmail;
    private javax.swing.JTextField digMembers_tfName;
    private javax.swing.JTextField digMembers_tfSearch;
    private javax.swing.JDialog dlgBooks;
    private javax.swing.JButton dlgBooks_btAdd;
    private javax.swing.JButton dlgBooks_btDelete;
    private javax.swing.JButton dlgBooks_btUpdate;
    private javax.swing.JComboBox<String> dlgBooks_comboGenre;
    private javax.swing.JLabel dlgBooks_lblId;
    private javax.swing.JList<Book> dlgBooks_lstBooksList;
    private javax.swing.JSpinner dlgBooks_spTotalCopies;
    private javax.swing.JTextField dlgBooks_tfAuthor;
    private javax.swing.JTextField dlgBooks_tfFilter;
    private javax.swing.JTextField dlgBooks_tfISBN;
    private javax.swing.JTextField dlgBooks_tfTitle;
    private javax.swing.JDialog dlgCheckout;
    private javax.swing.JButton dlgCheckout_btBack;
    private javax.swing.JButton dlgCheckout_btBack2;
    private javax.swing.JButton dlgCheckout_btBack3;
    private javax.swing.JButton dlgCheckout_btFinish;
    private javax.swing.JButton dlgCheckout_btNext1;
    private javax.swing.JButton dlgCheckout_btNext2;
    private javax.swing.JLabel dlgCheckout_lblBookDesc;
    private javax.swing.JLabel dlgCheckout_lblMemberDesc;
    private javax.swing.JList<Book> dlgCheckout_lstBookList;
    private javax.swing.JList<Member> dlgCheckout_lstMemberList;
    private javax.swing.JTabbedPane dlgCheckout_tabpane;
    private javax.swing.JDialog dlgLoaned;
    private javax.swing.JComboBox<String> dlgLoaned_comboShow;
    private javax.swing.JComboBox<String> dlgLoaned_comboSortby;
    private javax.swing.JList<Loan> dlgLoaned_lstLoanList;
    private javax.swing.JDialog dlgMembers;
    private javax.swing.JLabel dlgMembers_lblId;
    private javax.swing.JList<Member> dlgMembers_lstMemberList;
    private javax.swing.JDialog dlgReturn;
    private javax.swing.JButton dlgReturn_btBack1;
    private javax.swing.JButton dlgReturn_btBack2;
    private javax.swing.JButton dlgReturn_btBack3;
    private javax.swing.JButton dlgReturn_btFinish;
    private javax.swing.JButton dlgReturn_btNext1;
    private javax.swing.JButton dlgReturn_btNext2;
    private javax.swing.JLabel dlgReturn_lblBookDesc;
    private javax.swing.JLabel dlgReturn_lblMemberDesc;
    private javax.swing.JList<Loan> dlgReturn_lstLoanList;
    private javax.swing.JList<Member> dlgReturn_lstMemberList;
    private javax.swing.JTabbedPane dlgReturn_tabpane;
    private javax.swing.JFileChooser filechooser;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    // End of variables declaration//GEN-END:variables
}
