/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Database {

    final private Connection dbConn;

    Database() throws SQLException {
        dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20library?user=root&password=root");

    } // constructor will connect to database, initialize dbConn

    ArrayList<Member> getAllMembers(String condition) throws SQLException {
        try {
            ArrayList<Member> td = new ArrayList<>();
            String sql = "select * from members ";
            if (!condition.equals("")) {
                sql += condition;
            }
            try ( PreparedStatement select = dbConn.prepareStatement(sql);  ResultSet resultSet = select.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    String email = resultSet.getString("email");
                    int maxBooksAllowed = resultSet.getInt("maxBooksAllowed");
                    //int currentBooksRented = resultSet.getInt("currentBooksRented");
                    byte[] phone = resultSet.getBytes("photo");
                    //InputStream is = aBlob.getBinaryStream(0, aBlob.length());
                    //BufferedImage imag=ImageIO.read(is);

                    Member t = new Member(id, name, email, maxBooksAllowed, phone);

                    td.add(t);
                }
            }
            return td;
        } catch (InvalidValueException | IllegalArgumentException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void addMember(Member member) throws SQLException, FileNotFoundException, IOException {
        try ( PreparedStatement insert = dbConn.prepareStatement("insert into members values(NULL,?,?,?,0,?)")) {            //id,name,email,maxBooksAllowed,currentBooksRented,photo
            insert.setString(1, member.getName());
            insert.setString(2, member.getEmail());
            insert.setInt(3, member.getMaxBooksAllowed());
            //FileInputStream fi=new FileInputStream("woman.jpg");
            //insert.setBinaryStream(5,fi,fi.available());
            insert.setBytes(4, member.getPhoto());
            insert.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void updateMember(Member member) throws SQLException {
        try ( PreparedStatement update = dbConn.prepareStatement("update members set name=?,email=?,maxBooksAllowed=?,photo=? where id=?")) {
            update.setString(1, member.getName());
            update.setString(2, member.getEmail());
            update.setInt(3, member.getMaxBooksAllowed());
            update.setBytes(4, member.getPhoto());
            update.setInt(5, member.getId());
            update.execute();

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void deleteMember(int id) throws SQLException {
        try ( PreparedStatement delete = dbConn.prepareStatement("delete from members where id=?")) {
            delete.setInt(1, id);
            delete.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    boolean memberEligibleCheckout(int id) throws SQLException {
        try ( PreparedStatement query = dbConn.prepareStatement("select maxBooksAllowed,currentBooksRented from members where id=?")) {
            query.setInt(1, id);
            try ( ResultSet resultSet = query.executeQuery()) {
                if (resultSet.next()) {
                    int max = resultSet.getInt("maxBooksAllowed");
                    int cur = resultSet.getInt("currentBooksRented");
                    if (max > cur) {
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
        return false;
    }

    ArrayList<Book> getAllBooks(String condition) throws SQLException {
        try {
            ArrayList<Book> td = new ArrayList<>();
            //id,isbn,authors,title,copiesTotal,currentCopiesRented,genre
            String sql = "select * from books ";
            if (!condition.equals("")) {
                sql += condition;
            }
            try ( PreparedStatement select = dbConn.prepareStatement(sql);  ResultSet resultSet = select.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String isbn = resultSet.getString("isbn");
                    String authors = resultSet.getString("authors");
                    String title = resultSet.getString("title");
                    int copiesTotal = resultSet.getInt("copiesTotal");
                    String genre = resultSet.getString("genre");

                    Book t = new Book(id, isbn, authors, title, copiesTotal, Book.Genre.valueOf(genre));

                    td.add(t);
                }
            } catch (SQLException ex) {
                throw new SQLException(ex.getMessage());
            }
            return td;
        } catch (InvalidValueException | IllegalArgumentException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void addBook(Book book) throws SQLException, FileNotFoundException, IOException {
        try ( PreparedStatement insert = dbConn.prepareStatement("insert into books values(NULL,?,?,?,?,0,?)")) {
            insert.setString(1, book.getIsbn());
            insert.setString(2, book.getAuthors());
            insert.setString(3, book.getTitle());
            insert.setInt(4, book.getCopiesTotal());
            insert.setString(5, book.getGenre().toString());
            insert.execute();
        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void updateBook(Book book) throws SQLException {
        try ( PreparedStatement update = dbConn.prepareStatement("update books set isbn=?,authors=?,title=?,copiesTotal=?,genre=? where id=?")) {
            update.setString(1, book.getIsbn());
            update.setString(2, book.getAuthors());
            update.setString(3, book.getTitle());
            update.setInt(4, book.getCopiesTotal());
            update.setString(5, book.getGenre().toString());
            update.setInt(6, book.getId());
            update.execute();

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    void deleteBook(int id) throws SQLException {
        try ( PreparedStatement delete = dbConn.prepareStatement("delete from books where id=?")) {
            delete.setInt(1, id);
            delete.execute();

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    boolean bookEligibleCheckout(int id) throws SQLException {
        try ( PreparedStatement query = dbConn.prepareStatement("select copiesTotal,currentCopiesRented from books where id=?")) {
            query.setInt(1, id);
            ResultSet resultSet = query.executeQuery();
            if (resultSet.next()) {
                int max = resultSet.getInt("copiesTotal");
                int cur = resultSet.getInt("currentCopiesRented");
                if (max > cur) {
                    return true;
                }
            }
            resultSet.close();

        } catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        } finally {
        }

        return false;
    }

    //void bookCheckout(Loan loan) throws SQLException {
    void bookCheckout(int memberID, int bookID, int dueDateNum) throws SQLException {
        try {
            dbConn.setAutoCommit(false);
            //check eligible of member and book
            ResultSet resultSet = dbConn.createStatement().executeQuery("select * from members where id=" + memberID + " and maxBooksAllowed>currentBooksRented");
            if (resultSet.next()) {
                dbConn.createStatement().execute("select * from members where id=" + memberID + " FOR UPDATE");
            } else {
                throw new SQLException("the member data has been changed.");
            }

            resultSet = dbConn.createStatement().executeQuery("select * from books where id=" + bookID + " and copiesTotal>currentCopiesRented");
            if (resultSet.next()) {
                dbConn.createStatement().execute("select * from books where id=" + bookID + " FOR UPDATE");
            } else {
                throw new SQLException("the book data has been changed.");
            }

            //insert into loans
            PreparedStatement insert = dbConn.prepareStatement("insert into loans values(NULL,?,?,?,?,NULL)");
            //id,isbn,authors,title,copiesTotal,currentCopiesRented,genre
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date today = new Date();
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DAY_OF_MONTH, dueDateNum);
            String dueDate = sdf.format(c.getTime());
            insert.setInt(1, memberID);
            insert.setInt(2, bookID);
            insert.setString(3, sdf.format(today));
            insert.setString(4, dueDate);
            insert.execute();
            insert.close();
            //update books
            PreparedStatement update = dbConn.prepareStatement("update books set currentCopiesRented= currentCopiesRented+1 where id=?");
            update.setInt(1, bookID);
            update.execute();
            //update.close();
            //update members
            update = dbConn.prepareStatement("update members set currentBooksRented= currentBooksRented+1 where id=?");
            update.setInt(1, memberID);
            update.execute();
            update.close();
            dbConn.commit();
        } catch (SQLException e) {
            if (dbConn != null) {
                try {
                    //System.err.print("Transaction is being rolled back");
                    dbConn.rollback();
                } catch (SQLException ex) {
                    throw new SQLException(ex.getMessage());
                }
            }
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException ex) {
                throw new SQLException(ex.getMessage());
            }
        }
    }

    //void bookReturn(Loan loan) throws SQLException {
    void bookReturn(Loan loan) throws SQLException {
        try {
            /*
            int memberID = 0, bookID = 0;
            ResultSet resultSet = dbConn.createStatement().executeQuery("select memberId,bookId from loans where id=" + loanID);
            if (resultSet.next()) {
                memberID = resultSet.getInt("memberId");
                bookID = resultSet.getInt("bookId");
            } else {
                throw new SQLException("Can not find this loan: " + loanID);
            }
            */

            dbConn.setAutoCommit(false);

            dbConn.createStatement().execute("select * from loans where id=" + loan.getId() + " FOR UPDATE");
            dbConn.createStatement().execute("select * from members where id=" + loan.getMember().getId() + " FOR UPDATE");
            dbConn.createStatement().execute("select * from books where id=" + loan.getBook().getId() + " FOR UPDATE");

            //update loans
            PreparedStatement update = dbConn.prepareStatement("update loans set dateReturned= Now() where id=?");
            update.setInt(1, loan.getId());
            update.execute();
            //update members
            update = dbConn.prepareStatement("update books set currentCopiesRented= currentCopiesRented-1 where id=?");
            update.setInt(1, loan.getBook().getId());
            update.execute();
            //update.close();
            //update books
            update = dbConn.prepareStatement("update members set currentBooksRented= currentBooksRented-1 where id=?");
            update.setInt(1, loan.getMember().getId());
            update.execute();
            update.close();
            dbConn.commit();
        } catch (SQLException e) {
            if (dbConn != null) {
                try {
                    //System.err.print("Transaction is being rolled back");
                    dbConn.rollback();
                } catch (SQLException ex) {
                    throw new SQLException(ex.getMessage());
                }
            }
        } finally {
            try {
                dbConn.setAutoCommit(true);
            } catch (SQLException ex) {
                throw new SQLException(ex.getMessage());
            }
        }
    }

    ArrayList<Loan> getAllLoans(String condition) throws SQLException {
        try {
            ArrayList<Loan> td = new ArrayList<>();
            //id,isbn,authors,title,copiesTotal,currentCopiesRented,genre
            String sql = "select \n"
                    + "l.id,l.memberId,l.bookId,l.dateBorrowed,l.dueDate,l.dateReturned\n"
                    + ",b.isbn,b.authors,b.title,b.copiesTotal,b.currentCopiesRented,b.genre\n"
                    + ",m.name,m.email,m.maxBooksAllowed,m.currentBooksRented,m.photo \n"
                    + "from loans l inner join books b on l.bookid=b.id\n"
                    + "inner join members m on l.memberid=m.id";
            if (!condition.equals("")) {
                sql += " where " + condition;
            }
            try ( PreparedStatement select = dbConn.prepareStatement(sql)) {
                ResultSet resultSet = select.executeQuery();
                while (resultSet.next()) {
                    int bookid = resultSet.getInt("bookId");
                    String isbn = resultSet.getString("isbn");
                    String authors = resultSet.getString("authors");
                    String title = resultSet.getString("title");
                    int copiesTotal = resultSet.getInt("copiesTotal");
                    String genre = resultSet.getString("genre");
                    Book book = new Book(bookid, isbn, authors, title, copiesTotal, Book.Genre.valueOf(genre));

                    int memberid = resultSet.getInt("memberId");
                    String name = resultSet.getString("name");
                    String email = resultSet.getString("email");
                    int maxBooksAllowed = resultSet.getInt("maxBooksAllowed");
                    byte[] phone = resultSet.getBytes("photo");
                    Member member = new Member(memberid, name, email, maxBooksAllowed, phone);

                    int id = resultSet.getInt("id");
                    Date dateBorrowed = resultSet.getDate("dateBorrowed");
                    Date dueDate = resultSet.getDate("dueDate");
                    Date dateReturned = resultSet.getDate("dateReturned");
                    Loan loan = new Loan(id, member, book, dateBorrowed, dueDate, dateReturned);

                    td.add(loan);
                }
                resultSet.close();
            } catch (SQLException ex) {
                throw new SQLException(ex.getMessage());
            }
            return td;
        } catch (InvalidValueException | IllegalArgumentException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
}
