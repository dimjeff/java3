/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;


public class Owner {
    private int id;
    private String name;
    private byte[] photo;
    private int carsOwned; // computed, not stored

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) throws InvalidValueException {
        if(name.length()>100 || 0>name.length())
            throw new InvalidValueException("The name must be 1-100");
        this.name = name;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setCarsOwned(int carsOwned) {
        this.carsOwned = carsOwned;
    }

    public Owner(int id, String name, byte[] photo, int carsOwned) throws InvalidValueException {
        setId(id);
        setName(name);
        setPhoto(photo);
        setCarsOwned(carsOwned);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public int getCarsOwned() {
        return carsOwned;
    }

    @Override
    public String toString() {
        return String.format("%d: %s owns %d cars", id, name,carsOwned);
    }    
    
}
class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}