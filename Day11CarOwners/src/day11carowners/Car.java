/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

/**
 *
 * @author ipd
 */
public class Car {
    private int id;
    private int ownerId; //Integer can set to null
    private String makeModel;
    private int prodYear;
    private String plates;
    private String ownerName; // computed, not stored in Car table

    public Car(int id, int ownerId, String makeModel, int prodYear, String plates, String ownerName) throws InvalidValueException {
        setId(id);
        setOwnerId(ownerId);
        setMakeModel(makeModel);
        setProdYear(prodYear);
        setPlates(plates);
        setOwnerName(ownerName);
    }

    
    
    public void setId(int id) {
        this.id = id;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public void setMakeModel(String makeModel) throws InvalidValueException {
        if(makeModel.length()>100 || 0>makeModel.length())
            throw new InvalidValueException("The meke model must be 1-100");
        this.makeModel = makeModel;
    }

    public void setProdYear(int prodYear) throws InvalidValueException {
        if(prodYear>2100 || 1900>prodYear)
            throw new InvalidValueException("The produce year must be 1900-2100");
        this.prodYear = prodYear;
    }

    public void setPlates(String plates) throws InvalidValueException {
        if(!plates.equals("") && !plates.matches("^[a-zA-Z\\s\\d]{6,10}$"))
            throw new InvalidValueException("The plates must be 6-10 chars digits and space");
        this.plates = plates;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public int getProdYear() {
        return prodYear;
    }

    public String getPlates() {
        return plates;
    }

    public String getOwnerName() {
        return ownerName;
    }

    @Override
    public String toString() {
        return String.format("%d: %s , %d, %s", id,makeModel,prodYear,plates.equals("")?"noplates":plates);
    }
    
    
}
