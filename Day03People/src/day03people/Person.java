package day03people;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person {
    // TODO: add getters and setters
    String name; // 1-100 characters, excluding ;
    int heightCm; // 0-250 cm
    Date dateOfBirth; // year must be 1900-2100 both inclusive
    
    public Person(String name, int heightCm, Date dateOfBirth) {
        setName(name);
        setHeight(heightCm);
        setDOB(dateOfBirth);
    }

    public void setName(String name) {
        //if (name.length() < 2 || name.length() > 50) {
        if(!name.matches("[^;]{1,10}")){
            throw new IllegalArgumentException("Name must be between 1-10 characters long.");
        }

        this.name = name;
    }

    public void setHeight(int height) {
        if (height < 1 || height > 250) {
            throw new IllegalArgumentException("Height must be between 1-250.");
        }
        this.heightCm = height;
    }
    
    public void setDOB(Date dateOfBirth){
        try {
            Date min = new SimpleDateFormat("yyyy-MM-dd").parse("1900-01-01");
            Date max = new SimpleDateFormat("yyyy-MM-dd").parse("2100-12-31");

            if(min.compareTo(dateOfBirth) * dateOfBirth.compareTo(max) < 0){
                throw new IllegalArgumentException("Date Of Birth must be between 1900-2100.");
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException("Date Of Birth must be between 1900-2100.");
        }
        this.dateOfBirth = dateOfBirth;
    }
    
    String getName() {
        return this.name;
    }

    int getHeight() {
        return this.heightCm;
    }
    
    Date getDOB() {
        return this.dateOfBirth;
    }
    
    @Override
    public String toString(){
        return String.format("%s is %dcm tall bron %s", this.name,this.heightCm,dateFormat.format(this.dateOfBirth));
    }
    
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    
    // helper method if you want to
    public boolean matches(String search) {
        if(this.name.indexOf(search)!=-1)
            return true;
        
        String strHeight = ""+this.heightCm;
        if(strHeight.indexOf(search)!=-1)
            return true;
        
        String strDOB = dateFormat.format(this.dateOfBirth);
        if(strDOB.indexOf(search)!=-1)
            return true;
        
        return false;
    }
}
